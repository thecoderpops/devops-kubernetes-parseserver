# DevOps-Kubernetes-ParseServer

SocialCops project with kubernetes automation for notification-module containers and parse-server containers.

Project requirements: "Taking an example of a containerized web application as a single microservice, say Parse Server Example, we want to deploy it on a cluster of machines. These can be virtual machines on the cloud or enterprise-level client-managed hardware. "

## ParseServer Deployment

The project consists of a parse server which is containerised using docker and deployed on Google's Kubernetes Cluster engine
with 1 replica each for the containers. 
Ease of clustered deployments, remote updates, easy remote debugging, monitoring can be 
done simply using Kubernetes, an orchestration tool for containers which proves scaling features in one command, 
is developer and production-friendly.




The running docker file can be seen in the below image:

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture6.JPG)

The running pods for the parse server, dashboard and mongodb can be seen:

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture3.JPG)


The running services for the parse server, dashboard and mongodb can be seen:

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture4.JPG)


Data added via POST request:

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture2.JPG)

Data retrieved via GET request:

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/pic1.JPG)


Response of dashboard at the moment (to be corrected):

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture5.JPG)

**For prodcution environment, many more replicas should be created and this project is a small example which shows how 
complex architectures in software development process can be easily managed whether on cloud or on-premise for clients.


## Notification Deployment

It's a deployment of my earlier project which returns notifications stored in MongoDB database using Node.js Express
framework. The UI part is skipped as of now and the public IP for the cluster is http://35.193.34.165:3001/

Usage: Just enter http://35.193.34.165:3001/getnotifications in the browser and notifications as a JSON response 
can be seen.

![Scheme](https://bitbucket.org/thecoderpops/devops-kubernetes-parseserver/raw/master/images/Capture7.JPG)